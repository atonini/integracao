/**
 * Criado em: 14 de set de 2020 21:16:45
 * Autor: Aline
 */
package sincronizacaoreceita;

/**
 * @author Aline
 *
 */
public class SincronizacaoReceitaException extends Exception {

	private static final long serialVersionUID = -746998660482071008L;

	public SincronizacaoReceitaException(String msg) {
		super(msg);
	}

}
