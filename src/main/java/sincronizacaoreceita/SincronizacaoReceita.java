/*
 * TODO 
 * 1- melhorar mensagens, usar dicionario
 * 2- Usar alguma biblioteca de leitura CSV (nunca usei nenhuma (exceto de Excel) mas existem algunas de código aberto.
 * 3- Separar as validações em outra camada, mas acredito que não era o intuito do teste.
 */
package sincronizacaoreceita;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.OutputStreamWriter;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class SincronizacaoReceita implements CommandLineRunner {

	private static final int PRIMEIRA_LINHA = 1;
	private static final int AGENCIA_INDEX = 0;
	private static final int CONTA_INDEX = 1;
	private static final int VALOR_INDEX = 2;
	private static final int STATUS_INDEX = 3;
	private static final int QUANTIDADE_COLUNAS_CABECALHO = 4;
	private static final String AGENCIA_REGEX = "[0-9][0-9][0-9][0-9]";
	private static final String CONTA_REGEX = "[0-9][0-9][0-9][0-9][0-9]-[0-9]";
	private static final String STATUS_PATTERN = "status";
	private static final String VALOR_PATTERN = "saldo";
	private static final String CONTA_PATTERN = "conta";
	private static final String RESULTADO = "resultado";
	private static final String PONTO_E_VIRGULA = ";";
	private static final String AGENCIA_PATTERN = "agencia";
	private static final String VAZIO = "";
	private static final String HIFEN = "-";

	private static final Logger LOGGER = Logger.getLogger(SincronizacaoReceita.class);

	public static void main(String[] args) {
		SpringApplication.run(SincronizacaoReceita.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		LOGGER.info("Iniciando processamento do arquivo");

		if (args.length > 0) {
			String arquivo = args[0];
			String linha = null;

			LOGGER.info(String.format("Iniciando processamento do arquivo: %s", arquivo));

			try (BufferedReader csvReader = new BufferedReader(new FileReader(arquivo))) {

				int countLinha = 0;
				String[] dados = null;
				boolean isAtualizado = false;

				FileOutputStream outputStream = new FileOutputStream("Resultado.csv");
				BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(outputStream));

				try {
					while ((linha = csvReader.readLine()) != null) {
						countLinha++;
						isAtualizado = false;

						try {
							// TODO poderia ser validado se a linha contém o
							// separador definido e o numero de colunas definido

							dados = linha.split(PONTO_E_VIRGULA);
							if (countLinha == PRIMEIRA_LINHA) {
								validarCabecalho(dados);
								continue;
							}
							isAtualizado = processarLinha(dados);

						} catch (SincronizacaoReceitaException e) {
							LOGGER.error(String.format("LINHA[%d] Erro ao atualizar conta", countLinha), e);

						} finally {
							String resultado = criarLinhaSaida(linha, countLinha, isAtualizado);
							bw.write(resultado);
							bw.newLine();
						}
					}
				} catch (Exception e) {
					LOGGER.error(String.format("LINHA[%d] Erro ao ler arquivo:" + arquivo, countLinha), e);
				} finally {
					bw.close();
				}
			}

		} else {
			LOGGER.error("Argumentos inválidos:" + args);
		}

		LOGGER.info("Fim processamento do arquivo");
	}

	private String criarLinhaSaida(String linha, int countLinha, boolean isAtualizado) {
		StringBuilder resultado = new StringBuilder();
		resultado.append(linha).append(PONTO_E_VIRGULA);

		if (countLinha > PRIMEIRA_LINHA) {
			resultado.append(isAtualizado);
		} else {
			resultado.append(RESULTADO);
		}
		return resultado.toString();
	}

	/*
	 * TODO: Poderia ser tratado a leitura dinamica das colunas, no caso de não
	 * respeitar o cabeçalho estabelecido no exemplo, status ser a primeira
	 * coluna. Nesse caso teria que primeiro identificar o cabeçalho e os
	 * indexes das colunas serem definidos em momento de execução.
	 */
	private void validarCabecalho(String[] dados) throws Exception {
		if (dados == null) {
			throw new Exception(String.format("Erro ao processar linha, está nula"));
		}

		if (dados.length != QUANTIDADE_COLUNAS_CABECALHO) {
			throw new Exception("Erro ao processar linha, cabeçalho possui quantidade de colunas divergentes");
		}

		if (!AGENCIA_PATTERN.equals(dados[AGENCIA_INDEX])) {
			throw new Exception("Erro ao processar linha, coluna agência incorreta");
		}

		if (!CONTA_PATTERN.equals(dados[CONTA_INDEX])) {
			throw new Exception("Erro ao processar linha, coluna conta incorreta");
		}

		if (!VALOR_PATTERN.equals(dados[VALOR_INDEX])) {
			throw new Exception("Erro ao processar linha, coluna valor incorreta");
		}

		if (!STATUS_PATTERN.equals(dados[STATUS_INDEX])) {
			throw new Exception("Erro ao processar linha, coluna status incorreta");
		}
	}

	private boolean processarLinha(String[] dados) throws Exception {
		boolean isAtualizado = false;

		String agencia = getAgencia(dados[AGENCIA_INDEX]);
		String conta = getConta(dados[CONTA_INDEX]);
		Double saldo = convertReaisToDouble(dados[VALOR_INDEX]);
		Status status = convertToStatusEnum(dados[STATUS_INDEX]);

		// TODO caso fosse uma API de fato, realizar uma validação de timeout
		isAtualizado = getReceitaService().atualizarConta(agencia, getContaFormatada(conta), saldo.doubleValue(),
				status.toString());

		return isAtualizado;
	}

	// TODO validar o digito verificador, contudo não havia a regra descrita
	// neste documento
	private String getConta(String conta) throws SincronizacaoReceitaException {
		if (conta != null && !conta.isEmpty()) {
			Pattern p = Pattern.compile(CONTA_REGEX);
			Matcher m = p.matcher(conta);
			boolean isContaValida = m.matches();

			if (isContaValida) {
				return conta;
			}
		}

		throw new SincronizacaoReceitaException("Conta inválida");

	}

	private String getAgencia(String agencia) throws SincronizacaoReceitaException {
		if (agencia != null && !agencia.isEmpty()) {
			Pattern p = Pattern.compile(AGENCIA_REGEX);
			Matcher m = p.matcher(agencia);
			boolean isAgenciaValida = m.matches();

			if (isAgenciaValida) {
				return agencia;
			}
		}

		throw new SincronizacaoReceitaException("Agencia inválida");
	}

	@Bean
	public ReceitaService getReceitaService() {
		return new ReceitaService();
	}

	// TODO Desconheço se o Sicredi trabalha com outros tipos de moeda e/ou
	// conversões cotações, mas caso o valor do saldo
	// viesse em outra moeda fazer conversão e outro tatamento de locale
	private double convertReaisToDouble(String string) throws Exception {
		double doubleValue = 0.0;

		if (string != null && string.length() > 0) {
			Number valor = NumberFormat.getInstance(getLocate()).parse(string);
			doubleValue = valor.doubleValue();
		} else {
			throw new Exception("Valor inválido para conversão");
		}
		return doubleValue;
	}

	private Status convertToStatusEnum(String string) throws Exception {
		Status status = null;
		if (string != null) {
			status = Status.valueOf(string);
		} else {
			throw new Exception("Valor inválido para conversão");
		}
		return status;
	}

	private Locale getLocate() {
		return new Locale("pt", "BR");
	}

	private String getContaFormatada(String conta) {
		return conta.replace(HIFEN, VAZIO);
	}
}